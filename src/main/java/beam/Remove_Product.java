package beam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Remove_Product extends StartEngine

{
	@Test(priority = 4,description = "Remove a product from the cart. Verify the change.")
	public void Remove_a_product_from_the_cart_Verify_the_change() throws Exception
	{
		driver.findElement(By.xpath("(//input[@value='Delete'])[2]")).click();
		  	Thread.sleep(2000);
		  	String autualUpdatedProductprice = driver.findElement(By.xpath("(//span[contains(@class,'a-size-medium a-color-price')])[3]")).getText(); 	
			String expectedupdatedProductprice = "  829.00";
			Assert.assertEquals(autualUpdatedProductprice, expectedupdatedProductprice);
		    System.out.println("Product is deleted and Price in the cart is updated and verified");
		   
		    }
}