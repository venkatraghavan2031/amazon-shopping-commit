package beam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Add_Product extends StartEngine

{
	@Test(priority = 2,description = "Increase a product in cart and verify the price.")
	  public static void Increase_ProductinCart_and_Verify() throws Exception
	  {
		  driver.findElement(By.id("a-autoid-0-announce")).click();
		  driver.findElement(By.id("dropdown1_3")).click();
			
		    Thread.sleep(2000);
			String autualUpdatedProductprice = driver.findElement(By.xpath("(//span[contains(@class,'a-size-medium a-color-price')])[3]")).getText();
			String expectedupdatedProductprice = "  33,996.00";
			Assert.assertEquals(autualUpdatedProductprice, expectedupdatedProductprice);
		    System.out.println("Product Price in the cart is updated and verified");    
	  }
}