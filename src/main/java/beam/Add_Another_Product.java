package beam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Add_Another_Product extends StartEngine

{
	@Test(priority = 3,description = "Add another product. Verify cart change.")
  public void Add_another_product_Verify_cart_change() throws Exception
  {
		driver.findElement(By.name("field-keywords")).sendKeys("pendrive");
		//System.out.println("search entered");
		driver.findElement(By.xpath("(//input[@class='nav-input'])[1]")).click();
		//System.out.println("search button is clicked");
		driver.findElement(By.xpath("(//h2[contains(@class,'a-size-mini a-spacing-none')])[1]")).click();
		//System.out.println("product selected");
		
		String currentActiveWindowRef = driver.getWindowHandle();
		Set<String> allWindowRef = driver.getWindowHandles(); //for getting the unique reference for current session 
		List<String> listRef = new ArrayList<String>();
		listRef.addAll(allWindowRef);
	    System.out.println("Title before switch "+driver.getTitle());
	    driver.switchTo().window(listRef.get(1));
	    System.out.println("Title after switch "+driver.getTitle());
	    driver.findElement(By.name("submit.add-to-cart")).click();
	    System.out.println("add cart is clicked");
	    driver.findElement(By.id("hlb-view-cart-announce")).click();
	    
	    String autualfinalupdatedProductprice = driver.findElement(By.xpath("(//span[contains(@class,'a-size-medium a-color-price')])[4]")).getText();
	    String expectedfinalupdatedProductprice = "  34,825.00";
	    Assert.assertEquals(autualfinalupdatedProductprice, expectedfinalupdatedProductprice);
	    System.out.println("Final Product Price in the cart is updated and verified");
	    }
}
